/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import javax.swing.JOptionPane;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 *
 * @author user
 */
public class Read {

    ArrayList dataSch = new ArrayList();

    StringBuilder dataFuse = new StringBuilder();
    StringBuilder dataTime = new StringBuilder();

    public ArrayList getDataSch() {
        return dataSch;
    }

    public void setDataSch(ArrayList dataSch) {
        this.dataSch = dataSch;
    }

    public StringBuilder getDataFuse() {
        return dataFuse;
    }

    public void setDataFuse(StringBuilder dataFuse) {
        this.dataFuse = dataFuse;
    }

    public StringBuilder getDataTime() {
        return dataTime;
    }

    public void setDataTime(StringBuilder dataTime) {
        this.dataTime = dataTime;
    }

    public void readCsv(File seleFile) throws FileNotFoundException, IOException, ClassNotFoundException, ParseException {
        String excelFilePath = seleFile.getPath();
        FileInputStream inputStream = new FileInputStream(new File(excelFilePath));
        Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet Sheet = workbook.getSheetAt(0);
        String indicator = "";
        int i = 0;
        String tempDate = null, temp4 = null, temp3 = null, temp2 = null, temp1 = null;
        DataFormatter dataFormatter = new DataFormatter();
        Iterator<Row> rowIterator = Sheet.rowIterator();
        while (rowIterator.hasNext()) {
            Row row = rowIterator.next();
            Iterator<Cell> cellIterator = row.cellIterator();
            while (cellIterator.hasNext()) {
                Cell cell = cellIterator.next();
                String cellValue = dataFormatter.formatCellValue(cell);
//              System.out.println("data ke "+i+" "+cellValue);
                i++;
                if (!cellValue.isEmpty()) {
                    if (cellValue.contains("Jan") || cellValue.contains("Feb")
                            || cellValue.contains("Mar") || cellValue.contains("Apr")
                            || cellValue.contains("May") || cellValue.contains("Jun")
                            || cellValue.contains("Jul") || cellValue.contains("Aug")
                            || cellValue.contains("Sep") || cellValue.contains("Oct")
                            || cellValue.contains("Nov") || cellValue.contains("Dec")) {
                        tempDate = cellValue + "-2021";

                    } else if (cellValue.contains("Voice Repeater")) {
                        dataSch.add(tempDate + ";" + temp4 + ";" + temp3 + ";" + temp2 + ";" + temp1 + ";" + cellValue);
//                        System.out.println(tempDate+" - "+temp4+" - "+temp3+" - "+temp2+" - "+temp1+" - "+cellValue); 
                        indicator = cellValue;
                        DB db = new DB();
                        db.saveDataSch(tempDate, temp4, temp3, temp2, temp1, cellValue);
                    } else {
                        temp4 = temp3;
                        temp3 = temp2;
                        temp2 = temp1;
                        temp1 = cellValue;
                    }
                }
            }
        }
        if (indicator.contains("Voice Repeater")) {
            JOptionPane.showMessageDialog(null, "Schedule has been added");
        } else {
            JOptionPane.showMessageDialog(null, "No Voice Repeater schedule in this file");
        }
//        System.out.println();
    }

    public void readTxt(File selectedfiles) throws FileNotFoundException, IOException, ClassNotFoundException, ParseException {
        StringBuilder buffDataTime = new StringBuilder();
        StringBuilder buffDataFuse = new StringBuilder();
        String bufferData, timeData, fuseData, statusCommand;
        float fuseDataFloat = 0;
//        float indicatorFuse = 0;
        StringBuffer s = new StringBuffer("-------------------------------------------------------------------------");
        StringBuffer commandPCU = new StringBuffer("PCU Tele Command");
        StringBuffer statCommand = new StringBuffer("# No Respond from device !!");
        StringBuffer statCommandBusy = new StringBuffer("# Server is Busy!!");
        StringBuffer statCommandNull = new StringBuffer("");

        DB db = new DB();
        try (
                 BufferedReader as = new BufferedReader(new FileReader(selectedfiles))) {
            String line;
            while ((line = as.readLine()) != null) {
//                System.out.println(line);
                bufferData = line;
                if (bufferData.contentEquals(s)) {
                    timeData = as.readLine();
//                    System.out.println(timeData);
                    if (timeData.substring(20, timeData.length()).contentEquals(commandPCU)) {
//                        System.out.println(timeData);
                        as.readLine();
                        statusCommand = as.readLine();
//                        System.out.println(statusCommand);
                        if (statusCommand.contentEquals(statCommand) || statusCommand.contentEquals(statCommandBusy) || statusCommand.contentEquals(statCommandNull)) {
//                            System.out.println("No Respond");
                        } else {
                            for (int j = 0; j <= 21; j++) {
                                as.readLine();
                            }
//                            String test = as.readLine() ;
                            fuseData = as.readLine();
//                            System.out.println(fuseData);
                            fuseData = fuseData.substring(46, 53);

                            fuseDataFloat = Float.parseFloat(fuseData);
                            if (fuseDataFloat < 300 && fuseDataFloat > 100) {
                                buffDataFuse.append(fuseDataFloat + ",");
                                buffDataTime.append(timeData.substring(0, 19) + ",");
//                            System.out.println(timeData.substring(0, 19));
//                                indicatorFuse = fuseDataFloat;
                                db.saveData(timeData.substring(0, 19), fuseDataFloat);
                            }

                        }
                    }

                }

            }
            as.close();
            setDataFuse(buffDataFuse);
            setDataTime(buffDataTime);
        }
        if (fuseDataFloat < 300 && fuseDataFloat > 100) {
            JOptionPane.showMessageDialog(null, "Log Data has been added");
        } else {
            JOptionPane.showMessageDialog(null, "No Voice Repeater telemetry in this file");
        }
    }
}
