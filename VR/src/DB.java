/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import javax.swing.table.DefaultTableModel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author user
 */
public class DB {

    ArrayList dataSchDB = new ArrayList();

    ArrayList dataTimeDB = new ArrayList();
    ArrayList dataFuseDB = new ArrayList();

    ArrayList dataDate = new ArrayList();
    ArrayList dataStartUTC = new ArrayList();
    ArrayList dataStopUTC = new ArrayList();
    ArrayList dataStartWIB = new ArrayList();
    ArrayList dataStopWIB = new ArrayList();
    ArrayList dataMission = new ArrayList();

    public ArrayList getDataDate() {
        return dataDate;
    }

    public ArrayList getDataStartUTC() {
        return dataStartUTC;
    }

    public ArrayList getDataStopUTC() {
        return dataStopUTC;
    }

    public ArrayList getDataStartWIB() {
        return dataStartWIB;
    }

    public ArrayList getDataStopWIB() {
        return dataStopWIB;
    }

    public ArrayList getDataMission() {
        return dataMission;
    }

    public ArrayList getDataSchDB() {
        return dataSchDB;
    }

    public ArrayList getDataTimeDB() {
        return dataTimeDB;
    }

    public void setDataTimeDB(ArrayList dataTimeDB) {
        this.dataTimeDB = dataTimeDB;
    }

    public ArrayList getDataFuseDB() {
        return dataFuseDB;
    }

    public void setDataFuseDB(ArrayList dataFuseDB) {
        this.dataFuseDB = dataFuseDB;
    }

    public void saveData(String time, float Fuse) throws ClassNotFoundException, ParseException {
//        String tang = time.substring(0, 10);
//        String ti = time.substring(11, time.length());
//        System.out.println(tang);
//        System.out.println(ti);

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
        Date parsedDate = dateFormat.parse(time);
        Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
        System.out.println(timestamp);

        try (Connection conn = connect();
                PreparedStatement pstat = conn.prepareStatement("INSERT INTO `data_fuse`VALUES (?,?)")) {
            pstat.setTimestamp(1, timestamp);
            pstat.setFloat(2, Fuse);

            pstat.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void saveDataSch(String tang, String temp4, String temp3, String temp2, String temp1, String mission) throws ClassNotFoundException, ParseException {
//        System.out.println(tang);
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
        Date parsedDate = sdf.parse(tang);
        Timestamp timestamp2 = new java.sql.Timestamp(parsedDate.getTime());
        System.out.println(timestamp2);

        try (Connection conn = connect();
                PreparedStatement pstat = conn.prepareStatement("INSERT INTO `data_sch`VALUES (?,?,?,?,?,?)")) {
            pstat.setTimestamp(1, timestamp2);
            pstat.setString(2, temp4);
            pstat.setString(3, temp3);
            pstat.setString(4, temp2);
            pstat.setString(5, temp1);
            pstat.setString(6, mission);
            pstat.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    public void displayData(String start, String stop) throws ClassNotFoundException, SQLException {
        try (Connection conn = connect();
                Statement stat = conn.createStatement()) {
            boolean hasResultSet = stat.execute("SELECT * FROM data_fuse where Time between '" + start + "' and '" + stop + "' ");
            if (hasResultSet) {
                ResultSet result = stat.getResultSet();
                ResultSetMetaData metaData = result.getMetaData();
                //display data
                while (result.next()) {
                    String Time = String.valueOf(result.getString("Time"));
                    String Fuse = String.valueOf(result.getFloat("Fuse"));
                    dataTimeDB.add(Time);
                    dataFuseDB.add(Fuse);
//                    System.out.println("Time = " + Time);
//                    System.out.println("Fuse = " + Fuse);

                }
            }
//            setDataFuseDB(dataFuseDB);
//            setDataTimeDB(dataTimeDB);
        }
    }

    public void displayDataSch(String start, String stop) throws ClassNotFoundException, SQLException {
        try (Connection conn = connect();
                Statement stat = conn.createStatement()) {
            boolean hasResultSch = stat.execute("SELECT * FROM data_sch where Date between '" + start + "' and '" + stop + "'");
            if (hasResultSch) {
                ResultSet result = stat.getResultSet();
                ResultSetMetaData metaData = result.getMetaData();
                //display data
                while (result.next()) {
                    String Date = String.valueOf(result.getString("Date"));
                    String StartUTC = String.valueOf(result.getString("StartUTC"));
                    String StopUTC = String.valueOf(result.getString("StopUTC"));
                    String StartWIB = String.valueOf(result.getString("StartWIB"));
                    String StopWIB = String.valueOf(result.getString("StopWIB"));
                    String Mission = String.valueOf(result.getString("Mission"));
                    dataDate.add(Date);
                    dataStartUTC.add(StartUTC);
                    dataStopUTC.add(StopUTC);
                    dataStartWIB.add(StartWIB);
                    dataStopWIB.add(StopWIB);
                    dataMission.add(Mission);
                }
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
    }

    //Create connection to DB
    public Connection connect() throws ClassNotFoundException {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            return DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "");

        } catch (SQLException | ClassNotFoundException e) {
            System.out.println(e);
            return null;
        }
    }

    void saveData(String substring) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
